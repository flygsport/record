<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class Claim extends CRUDOperation {

  private $valueFields = array(
    'class_id' => 'classId',
    'subclass_id' => 'subclassId',
    'type_id' => 'typeId',
    'category_id' => 'categoryId',
    'zone_id' => 'zoneId',
    'group_id' => 'groupId',
    'performance_date' => 'performanceDate',
    'claim_nbr_gf' => 'claimNbrGF',
    'claim_nbr_fsf' => 'claimNbrFSF',
    'claim_nbr_fai' => 'claimNbrFAI',
    'status_gf' => 'statusGF',
    'status_fsf' => 'statusFSF',
    'status_fai' => 'statusFAI',
    'current_record' => 'currentRecord',
    'location' => 'location',
    'performance' => 'performance',
    'comment' => 'comment',
    'within_sweden' => 'withinSweden',
    'first_record' => 'firstRecord');
  
  private $readOnlyFields = array(
    'creation_time' => 'creationTime',
    'last_edit' => 'lastEdit');
  
  function __construct(){
    $this->table = 'record_claim';
    $this->primaryKeys = array('claim_id' => 'claimId');
    $this->autoincrementField = 'claim_id';
    $this->timestampField = 'creation_time';
    $this->orderBy = 'performance_date DESC, claim_nbr_fsf DESC';
    $this->publicFields = array_merge( $this->primaryKeys, $this->valueFields, $this->readOnlyFields );
  }
  
  //=== replace() =============================================================
  function replace(){
    Authenticate::authorize();
    
    $this->executeUpdate($this->table, $this->valueFields, $this->primaryKeys);

    $tmp = array();
    foreach($this->primaryKeys as $db => $json){
      $tmp[$json] = $this->parameters[$json];
    }
    $this->parameters = $tmp;
    $this->read();
  }

}

(new Claim())->generateResponse();

?>

<?php
require_once __DIR__ . '/include/authenticate.php';
require_once (__DIR__ . '/include/CRUDOperation.php');

class Claim extends CRUDOperation
{
  function __construct(){
    $this->table = 'claimant';
    $this->primaryKeys = array('claim_id' => 'claimId', 'person_id' => 'personId');
    $this->publicFields = array('claim_id' => 'claimId', 'person_id' => 'personId', 'club_id' => 'clubId');
  }
  
  public function replace() {
    throw new RESTfulException ( 'PUT is not supported', 'PUT is not supported', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
  }
  
  public function read() {
    
    // Authenticate::authorize();
    $claimId = $this->getParameter('claimId');
    $query = 'SELECT * FROM claimant, club, person where '
      .' claimant.club_id=club.club_id and claimant.person_id=person.person_id and claim_id=?'
      .' ORDER BY family_name, given_name, date_of_birth';
    $stmt = $this->mysqli->prepare($query);
    $stmt->bind_param("s", $claimId);
    $stmt->execute();
    $res = $stmt->get_result();
    
    while ($row = $res->fetch_assoc()) {
      $res_row = array();
      $res_row['claimId'] = $row['claim_id'];
      
      $person = array();
      $person['personId'] = $row['person_id'];
      $person['faiId'] = $row['fai_id'];
      $person['personIdIOL'] = $row['iol_person_id'];
      $person['givenName'] = $row['given_name'];
      $person['familyName'] = $row['family_name'];
      $person['dateOfBirth'] = $row['date_of_birth'];
//      if ($this->check_privilege('office')) {
//        $person['pnrDigits'] = $row['pnr_digits'];
//      }
      $person['clubId'] = $row['club_id'];
      // TODO wrong club_id
      $res_row['person'] = $person;
      
      $club = array();
      $club['clubId'] = $row['club_id'];
      $club['clubIdIOL'] = $row['iol_club_id'];
      $club['name'] = $row['name'];
      $club['startDate'] = $row['start_date'];
      $club['shutDownDate'] = $row['shut_down_date'];
      $res_row['club'] = $club;
      
      $this->result[] = $res_row;
    }
  }
 
}

(new Claim())->generateResponse();

?>

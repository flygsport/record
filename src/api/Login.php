<?php

/**
 * Manage user credentials. No log in session data is stored on the server.
 * Autentication is done by sending a jwt token in the 'Authorization' 
 * http header. The value for the header must be a string starting with 
 * 'Bearer ', followed by the jwt token.
 * 
 * Use logion.php to get the jwt token when the user logs in. Store it in the 
 * browser using Javascript, and/or localstore.
 * 
 * Use http POST to get a jwt token that later can be used to authenticate the 
 * user.
 * parameters (json object in the http body): 
 *   {user: username, password: password}
 * return: a json object:
 *   {ok: true, data: jwt_token}
 * 
 * Use http PATCH to change password.
 * parameters (json object in the http body): 
 *   {user: username, password: password, old_password: old_password}
 * return: a json object:
 *   {ok: true, data: {}}
 * 
 */
require_once __DIR__ . '/vendor/autoload.php';
require_once (__DIR__ . '/include/DBOperation.php');
use \Firebase\JWT\JWT;

class Login extends RESTfulOperation {
  
  const lifetime = 2*60*50; // lifetime of jwt-token: 2 hours
  
  // login, http POST
  public function create() {
    $user = $this->getParameter ( 'user' );
    $password = $this->getParameter ( 'password' );
    $this->result = $this->validate_user_password($user, $password);  // throws exception if missmatch
    $this->result['jwt'] = $this->create_jwt($user);
  }

  // change password, http PATCH
  function update() {
    $user = $this->getParameter ( 'user' );
    $password = $this->getParameter ( 'password' );
    $old_password = $this->getParameter ( 'old_password' );
    
    $this->validate_user_password($user, $old_password);  // throws exception if missmatch
    
    $hash = password_hash($password, PASSWORD_DEFAULT);
    $query = 'UPDATE user set password=? WHERE user=?';
    $stmt = $this->mysqli->prepare ( $query );
    $stmt->bind_param ( "ss", $hash, $user );
    $stmt->execute ();
    $row_cnt = $stmt->affected_rows;
    
    if($row_cnt != 1){
      throw new RESTfulException("failed to update password, database error",
        "failed to update password, database error");
    }
  }
  

  /**
   * Verify a (user, password) pair. Throws an exception if the credentials are bad.
   */
  private function validate_user_password($user, $password) {
    // check that the server support bcrypt
    if (!(defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH)) {
      throw new RESTfulException("CRYPT_BLOWFISH is not supported by the server",
        "CRYPT_BLOWFISH is not supported by the server",
        RESTfulException::HTTP_STATUS_SERVICE_UNAVAILABLE);
    }
    
    $query = 'SELECT user, password, name, email FROM user WHERE user=?';
    $stmt = $this->mysqli->prepare ( $query );
    $stmt->bind_param ( "s", $user );
    $stmt->execute ();
    $res = $stmt->get_result ();
    
    if($row = $res->fetch_assoc ()){
      $hash = $row['password'];
      if (!password_verify($password, $hash)) {
        // password do not match
        $this->failed_authentication();
      }
    } else {
      // no user
      $this->failed_authentication();
    }
    unset($row['password']);
    return $row;
    // we have a valid (user, password).
  }
  
  private function create_jwt($user_id) {
    Global $privateKey;
    
    date_default_timezone_set('Europe/Stockholm');
    $issuedAt   = time();
    $expire     = $issuedAt + self::lifetime;
    $token = array(
      "iss" => "flygsport.eu",
      "sub" => $user_id,               // subject == user id
      "exp" => $expire            // Expiration Time
    );
    $jwt = JWT::encode($token, $privateKey, 'RS256');
    return print_r($jwt, true);
  }
  
  private function failed_authentication() {
    throw new RESTfulException("unknown user or wrong password",
                               "unknown user or wrong password",
                               RESTfulException::HTTP_STATUS_BAD_REQUEST,
                               "bad credentials");
  }
    
  
}

(new Login())->generateResponse();

?>

<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class Person extends CRUDOperation {

  function __construct(){
    $this->table = 'person';
    $this->orderBy = 'family_name, given_name';
    $this->autoincrementField = 'person_id';
    $this->publicFields = array(
      'person_id' => 'personId', 
      'fai_id' => 'faiId', 
      'fai_license' => 'faiLicence', 
      'iol_person_id' => 'iolId', 
      'given_name' => 'givenName', 
      'family_name' => 'familyName', 
      'date_of_birth' => 'dateOfBirth', 
      'pnr_digits' => 'pnrDigits', 
      'club_id' => 'clubId' );
  }
  
}

(new Person())->generateResponse();

?>

<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordCategory extends CRUDOperation {

  function __construct(){
    $this->table = 'record_category';
    $this->orderBy = 'category_text';
    $this->publicFields = array(
      'category_id' => 'categoryId',
      'category_text' => 'categoryText'
    );
  }
  
}

(new ReordCategory())->generateResponse();

?>

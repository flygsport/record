<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordClass extends CRUDOperation {

  function __construct(){
    $this->table = 'record_class';
    $this->autoincrementField = 'class_id';
    $this->orderBy = 'class_abbrev';
    $this->publicFields = array(
      'class_id' => 'classId', 
      'class_abbrev' => 'classAbbrev', 
      'class_name' => 'className'
    );
  }
  
}

(new ReordClass())->generateResponse();

?>

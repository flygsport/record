<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordClass extends CRUDOperation {

  function __construct(){
    $this->table = 'club';
    $this->orderBy = 'name';
    $this->publicFields = array(
      'club_id' => 'clubId',
      'iol_club_id' => 'iolClubId',
      'name' => name,
      'start_date' => 'startDate',
      'shut_down_date' => 'shutDownDate');
  }
  
}

(new ReordClass())->generateResponse();

?>

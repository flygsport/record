<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordGroup extends CRUDOperation {

  function __construct(){
    $this->table = 'record_group';
    $this->autoincrementField = 'group_id';
    $this->publicFields = array(
      'group_id' => 'groupId',
      'group_text' => 'groupText',
      'class_id' => 'classId'
    );
    $this->orderBy = 'group_text';
  }
  
}

(new ReordGroup())->generateResponse();

?>

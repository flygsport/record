<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordSubclass extends CRUDOperation {

  function __construct(){
    $this->table = 'record_subclass';
    $this->autoincrementField = 'subclass_id';
    $this->publicFields = array(
      'subclass_id' => 'subclassId',
      'subclass_abbrev' => 'subclassAbbrev',
      'subclass_name' => 'subclassName',
      'class_id' => 'classId'
    );
    $this->orderBy = 'subclass_abbrev';
  }
  
}

(new ReordSubclass())->generateResponse();

?>

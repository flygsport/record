<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordType extends CRUDOperation {

  function __construct(){
    $this->table = 'record_type';
    $this->autoincrementField = 'type_id';
    $this->orderBy = 'type_text';
    $this->publicFields = array(
      'type_id' => 'typeId',
      'type_text' => 'typeText',
      'class_id' => 'classId'
    );
  }
  
}

(new ReordType())->generateResponse();

?>

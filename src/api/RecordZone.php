<?php
require_once (__DIR__ . '/include/CRUDOperation.php');

class ReordZone extends CRUDOperation {

  function __construct(){
    $this->table = 'record_zone';
    $this->orderBy = 'zone_text';
    $this->publicFields = array(
      'zone_id' => 'zoneId',
      'zone_text' => 'zoneText'
    );
  }
  
}

(new ReordZone())->generateResponse();

?>

<?php
require_once __DIR__ . '/authenticate.php';
require_once (__DIR__ . '/DBOperation.php');

/**
 * Provide CRUD for a RESTy access to a database table.
 * 
 * --- READ --- 
 * Returns all columns listed in $publicFields. 
 * Supports filter by any fields (and, all values must match):
 * example: http://localhost/record/api/Claim.php?classId=50&subclassId=26
 * 
 * 
 * === Parameters ===
 * Protected members, set their values in the constructor.
 * 
 * $table is the name of the database table to access
 * 
 * $primaryKeys is an array of the primary keys for this table, 'database column name' => 'REST api parameter name'
 * 
 * $publicFields is an array all values returned by GET when not logged in, 'database column name' => 'REST api parameter name'
 * 
 * $orderBy is a text used in the SQL query for the ORDER BY value. Set to null if no order: " ORDER BY $this->orderBy"
 * 
 * Example:
 *     $this->table = 'record_claim';
 *     $this->orderBy = 'performance_date DESC';
 *     $this->publicFields = array(
 *          'claim_id' => 'claimId', 
 *          'class_id' => 'classId',
 *          ...
 *          );

 */
class CRUDOperation extends RESTfulOperation {

  protected $publicFields = array();
  protected $table = '';
  protected $primaryKeys = null;
  protected $orderBy = null;
  protected $timestampField = null;
  
  protected $query = "";
  protected $values = array();
  
  //=== read() =============================================================
  public function read() {
    $this->executeSelect($this->table, $this->publicFields, $this->publicFields, $this->orderBy);
  }
  
  //=== delete() =============================================================
  public function delete() {
    Authenticate::authorize();
    $this->executeDelete($this->table, $this->primaryKeys);
  }
  
  //=== replace() =============================================================
  function replace(){
    Authenticate::authorize();
    if($this->primaryKeys == null){
      throw new RESTfulException ( 'unknown primary key', 'unknown primary key',
        RESTfulException::HTTP_STATUS_INTERNAL_SERVER_ERROR, false );
    }
    $this->executeUpdate($this->table, $this->publicFields, $this->primaryKeys);
    // Read the updated rows for the reply
    // NOTE! if there is a timestamp in the row, remove all but the promary keys from the parameters
    // (else no row will match)
    // $this->parameters = array();
    $this->read();
  }
  
  //=== create() =============================================================
  function create(){
    Authenticate::authorize();
    $tmp = $this->publicFields;
    if($this->timestampField != null){
      unset($tmp[$this->timestampField]);
    }
    if($this->autoincrementField != null){
      unset($tmp[$this->autoincrementField]);
    }
    $id = $this->executeInsert($this->table, $tmp, $this->timestampField);
    // Read the new row for the reply
    $this->parameters = array();
    if($this->autoincrementField == null){
      foreach($this->primaryKeys as $db => $json){
        $this->parameters[$json] = $tmp[$json];
      }
    } else {
      // find the API name of the autoincrement
      foreach($this->publicFields as $db => $json){
        if($this->autoincrementField == $db){
          $this->parameters[$json] = $id;
        }
      }
    }
    $this->read();
  }
  
  /**
   * Append a comma separated list of "database-column-name AS rest-api-name" to $query. 
   * Commonly used for the field list in a SELECT statement. 
   * The generated list renames the columns in the sql-answer to correct names for the returned REST-json object.
   */
  protected function buildSelectNames($nameMap, &$query){
    $separator = ' ';
    foreach($nameMap as $db => $json){
      $query .= "$separator $db AS $json";
      $separator = ',';
    }
  }
  
  /**
   * Append a list of 'field=?' to $query
   * Use ',' as separator for the UPDATE/INSERT, or 'and' as separator for the WHERE part
   * The values are added to $value. Pass this array to mysqli->bind_param().
   */
  protected function buildQueryValues($nameMap, &$query, &$values, $separator = ','){
    $sep = '';
    foreach($nameMap as $db => $json){
      $value = $this->getParameterOrNull($json);
      if($value != null){
        // create a dynamic variable, needed since bind_param needs references (do not accept values)
        ${"__sqlParameterValue$json"} = $value;
        $values[] = &${"__sqlParameterValue$json"};  // add a reference to the array
        $query .= " $sep $db=?";
        $sep = $separator;
      }
    }
  }
  
  protected function executeSQL(&$stmt){
    $stmt->execute();
    $res = $stmt->get_result();
    
    while ($row = $res->fetch_assoc()) {
      $res_row = array();
      foreach($row as $key => $value){
        $res_row[$key] = $value;
      }
      $this->result[] = $res_row;
    }
  }
  
  //=== executeDelete() =============================================================
  protected function executeDelete($table, $primaryKeys){
    $values = array();
    
    $condStr = ' ';
    $separator = '';
    foreach($primaryKeys as $db => $json){
      $condStr .= "$separator $db=?";
      $separator = ' and ';
      $value = $this->getParameter($json);
      ${"__deleteValue$json"} = $value;
      $values[] = &${"__deleteValue$json"};
    }
    
    $query = "DELETE FROM $table WHERE $condStr";
    
    $stmt = $this->mysqli->prepare($query);
    
    array_unshift($values, str_repeat("s", count($values)));
    $ref = new ReflectionClass('mysqli_stmt');
    $method = $ref->getMethod("bind_param");
    $method->invokeArgs($stmt, $values);
    $stmt->execute();
    $affectedRows = $this->mysqli->affected_rows;
    
    // return the updated database rows
    $this->result['affectedRows'] = $affectedRows;
  }
  
  //=== executeSelect() =============================================================
  protected function executeSelect($table, &$fieldMap, &$filterMap=null, $orderBy=null) {
//    Authenticate::authorize();
    
    $values = array();
    $query = "SELECT";

    $this->buildSelectNames($fieldMap, $query);

    $query .= " FROM $table";

    // filer
    $filter = '';
    $this->buildQueryValues($filterMap, $filter, $values, 'and');
    if(!empty($values)){
      $query .= " WHERE $filter";
    }
 
    // order by
    if($orderBy != null){
      $query .=  ' ORDER BY ' . $orderBy;
    }

    $stmt = $this->mysqli->prepare($query);
    
    if(!empty($values)){
      array_unshift($values, str_repeat("s", count($values)));
      $ref = new ReflectionClass('mysqli_stmt');
      $method = $ref->getMethod("bind_param");
      $method->invokeArgs($stmt, $values);
    }
    
    $this->executeSQL($stmt);
  }

  //=== executeUpdate() =============================================================
  function executeUpdate($table, &$updateFields, $conditionKeys){
    $values = array();
    $query = "UPDATE";

    $assignStr = ' ';
    $separator = '';
    foreach($updateFields as $db => $json){
      $assignStr .= "$separator $db=?";
      $separator = ', ';
      $value = $this->getParameter($json);
      ${"__assignValue$json"} = $value;
      $values[] = &${"__assignValue$json"};
    }
    
    $condStr = ' ';
    $separator = '';
    foreach($conditionKeys as $db => $json){
      $condStr .= "$separator $db=?";
      $separator = ' and ';
      $value = $this->getParameter($json);
      ${"__conditionValue$json"} = $value;
      $values[] = &${"__conditionValue$json"};
    }
    
    $query = "UPDATE $table SET $assignStr WHERE $condStr";

    $stmt = $this->mysqli->prepare($query);
    
    array_unshift($values, str_repeat("s", count($values)));
    $ref = new ReflectionClass('mysqli_stmt');
    $method = $ref->getMethod("bind_param");
    $method->invokeArgs($stmt, $values);
    $stmt->execute();
    $affectedRows = $this->mysqli->affected_rows;
    
    // return the updated database rows
    $this->result['affectedRows'] = $affectedRows;
  }

  //=== executeInsert() =============================================================
  function executeInsert($table, &$setFields, $timestampField=null){
    $values = array();
    $query = "INSERT INTO $table SET";
    
    $separator = '';
    foreach($setFields as $db => $json){
      if($db != $this->autoincrementField){
        $query .= "$separator $db=?";
        $separator = ', ';
        $value = $this->getParameter($json);
        ${"__setValue$json"} = $value;
        $values[] = &${"__setValue$json"};
      }
    }
    
    if($timestampField){
      $query .= ", $timestampField=now()";
    }
    
    $stmt = $this->mysqli->prepare($query);
    
    array_unshift($values, str_repeat("s", count($values)));
    $ref = new ReflectionClass('mysqli_stmt');
    $method = $ref->getMethod("bind_param");
    $method->invokeArgs($stmt, $values);
    $stmt->execute();
    return $this->mysqli->insert_id;
  }

}

?>

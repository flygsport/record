<?php
require_once __DIR__ . '/../vendor/autoload.php';
use \Firebase\JWT\JWT;

class Authenticate {
  const headerName = "Auth";
  
  public static function authorize() {
    Global $publicKey;  // RS256 public key
    
    date_default_timezone_set('Europe/Stockholm');
    $headers = apache_request_headers();
    if(isset($headers[self::headerName])){
      $jwt = $headers[self::headerName];
      $pieces = explode(" ", $jwt); // "Bearer JWT"
      $tmp = $pieces[0];
      if(strcmp($tmp, "Bearer") !== 0){
        throw new RESTfulException('illegal value for Authorization http header', 
          'illegal value for Authorization http header, expected "Bearer ", found: ' . $tmp, RESTfulException::HTTP_STATUS_UNAUTHORIZED);
      }
      $auth = JWT::decode($pieces[1], $publicKey, array('RS256'));
    } else {
      
      throw new RESTfulException("unauthorized operation", 'unauthorized operation, missing Authorization header. $headers='
        . json_encode($headers), RESTfulException::HTTP_STATUS_UNAUTHORIZED);
    }
  }
  
}

?>

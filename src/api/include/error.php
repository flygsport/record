<?php
/*********************************************************************************
 * Setup error handling for server side services generating JSON data.
 * Catches compile and runtime errors and warnings and wrap them into a JSON object.
 * The resulting JSON object is printed using echo (sent in the http response package)
 * @author Per Andersson
  **/

/**
 *  Custom error handler. Wrap the error into an JSON object, prints it and die().
 * @param
 *        	$errno
 * @param
 *        	$errstr
 * @param
 *        	$errfile
 * @param
 *        	$errline
 **/
function myErrorHandler($errno, $errstr, $errfile, $errline) {
	if($errno == 8192) {return;} //"Automatically populating $HTTP_RAW_POST_DATA is deprecated and will be removed in a future version. To avoid this warning set 'always_populate_raw_post_data' to '-1' in php.ini and use the php:\/\/input stream instead."
	$response = array (
			'errkind' => 'php',
			'errno' => $errno,
			'errstr' => $errstr,
			'errfile' => $errfile,
			'errline' => $errline
	);
	echo json_encode ( $response );
	die ();
}

function fatal_handler() {
	$error = error_get_last();
	if( $error !== NULL) {
		$errno   = $error["type"];
		$errfile = $error["file"];
		$errline = $error["line"];
		$errstr  = $error["message"];
		myErrorHandler($errno, $errstr, $errfile, $errline);
	}
}

/**
 * Install custom error handler to generate a json http response when an error occurs.
 */
set_error_handler ( 'myErrorHandler' );
//register_shutdown_function( "fatal_handler" );

// ensure JSON output. The default warning/error message printed to stdout, "WARNING ...",
// will cause a parse error in the receiving javascript, i.e. $.getJSON(...) will generate an uncatchable error
error_reporting(E_ALL & ~ E_ERROR);
?>

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { MatToolbarModule, MatSidenavModule, MatIconModule, MatDialogModule,
         MatSelectModule, MatInputModule, MatDatepickerModule, MatCheckboxModule, 
         MatCardModule, MatButtonModule, MatSnackBarModule, MatExpansionModule,
         MatTabsModule, MatSortModule, MatAutocompleteModule } from '@angular/material';

import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE } from '@angular/material/core';

import { AppComponent } from './app.component';
import { SidenavModule } from './sidenav/sidenav.module';
import { AboutComponent } from './about/about.component';
import { UserModule } from './user/user.module';

import { HttpErrorLogger } from "./http.error.logger";
import { AuthenticationModule } from './auth/auth.module';
import { ListComponent } from './list/list.component';
import { ShowClaimComponent } from "./show.claimants/show.claimants.component";
import { RecordService } from "./record.service";
import { ClaimComponent } from './claim/claim.component';
import { EditClaimantsComponent } from "./edit.claimants/edit.claimants.component";
import { RecordModule } from "./record-components/record-components.module";
import { ConfirmDialogComponent } from "./dialogs/confirm.dialog";

//import { AuthenticateInterceptor } from  './authenticate.interceptor';
//import { AuthenticationService } from "../services/authentication.service";

const appRoutes: Routes = [
                           { path: 'list', component: ListComponent },
                           // three different functions of the same component. Add three url paths for clearity.
                           { path: 'editClaim/:id', component: ClaimComponent },
                           { path: 'showClaim/:id', component: ClaimComponent },
                           { path: 'newClaim', component: ClaimComponent },
                           { path: '',
                               redirectTo: '/list',
                               pathMatch: 'full'
                             },
                           { path: '**', component: AboutComponent }
                         ];



@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ListComponent,
    ShowClaimComponent,
    EditClaimantsComponent,
    ClaimComponent,
    ConfirmDialogComponent
  ],
  entryComponents: [ ConfirmDialogComponent ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    
    AuthenticationModule,
    RecordModule,
    SidenavModule,
    UserModule,
    
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatSidenavModule, 
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTabsModule,
    MatToolbarModule
  ],
  providers: [
    RecordService,
    {provide: MAT_DATE_LOCALE, useValue: 'sv-SE'},  // locale svenska-Sverige (used by datepicker)
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorLogger,
      multi: true,
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }

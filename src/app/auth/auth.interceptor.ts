import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from "./auth.service";

/**
 * AuthenticateInterceptor adds an Authorization http header to all aoutgoing http requests.
 * The hader follows Auth0 syntax
 * The value of the header is: 'Bearer ' + JWT-token
 */

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  static readonly authHeaderName = 'Auth';
  
  constructor(private injector: Injector){
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Get the auth header from the service.
    // Getting the uthenticationService in the constructor will cause a cyclic injector dependency
    const auth = this.injector.get(AuthenticationService);
    const authHeader = auth.getAuthorizationHeader();
    // if we have an authetication token, add it to the request
    if(authHeader){
      req = req.clone({headers: req.headers.set(AuthenticationInterceptor.authHeaderName, 'Bearer ' + authHeader)});
    }
    return next.handle(req);
  } 
  
}
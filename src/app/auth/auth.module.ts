import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from "@angular/common/http";

import { AuthenticationService } from "./auth.service";
import { AuthenticationInterceptor } from  './auth.interceptor';

@NgModule({
  imports: [  ],
  declarations: [  ],
  exports: [ ],
  providers: [ 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
    AuthenticationService
  ]
})
export class AuthenticationModule { }

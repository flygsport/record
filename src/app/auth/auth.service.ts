import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from "rxjs/BehaviorSubject";
 
@Injectable()
export class AuthenticationService {
  public token: string = null;   // JWT auth token 
  public user: string = null;   // JWT auth token 
  private userStream = new BehaviorSubject(null);
 
  constructor(private http: HttpClient) {
    // set token if saved in local storage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser){
      this.token = currentUser.jwt;
      this.userStream.next(currentUser);
    }
  }
 
  public getAuthorizationHeader(): string{
    return this.token;
  }
  public isLoggedIn(): boolean{
    return this.token !== null;
  }
  public getUserValue(){
    return this.user;
  }
  
  public getUser(): Observable<{ user: string, name: string, email: string, jwt: string }>{
    return this.userStream;
  }
  

  public login(username: string, password: string): Observable<boolean> {
    return this.http.post('/record/api/Login.php', {user: username, password: password} )
            .map(response => {
                if (response['ok']) {
                  this.user = response['data'];
                  // there's a jwt token in the response
                  let token = response['data'].jwt;
                  this.token = token;
                  // store username and jwt token in local storage to keep user logged in between page refreshes
                  localStorage.setItem('currentUser', JSON.stringify(this.user));
                  this.userStream.next(this.user);
 
                  // return true to indicate successful login
                  return true;
                } else {
                  // return false to indicate failed login
                  this.userStream.next(null);
                  return false;
                }
            });
  }

  public logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    this.userStream.next(null);
  }

}
import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Params } from "@angular/router";
import { RecordService } from "../record.service";
import { Claim, RecordClass, RecordSubclass, nullClaim, RecordType, RecordCategory, RecordZone, RecordGroup } from "../types/record.types";
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from "../auth/auth.service";

import { MatSnackBar, MatDialog } from '@angular/material';
import { RecordSubclassComponent } from "../record-components/record-subclass/record-subclass.component";
import { RecordTypeComponent } from "../record-components/record-type/record-type.component";
import { RecordGroupComponent } from "../record-components/record-group/record-group.component";
import { RecordClassComponent } from "../record-components/record-class/record-class.component";

@Component({
  selector: 'air-claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.scss']
})
export class ClaimComponent implements OnInit {
  public claim: Claim = nullClaim();
  public isLoggedIn: boolean = false; 

  public recordClasses: Observable<RecordClass[]> = null;
  public recordSubclasses: Observable<RecordSubclass[]> = null;
  public recordTypes: Observable<RecordType[]> = null;
  public recordGroups: Observable<RecordGroup[]> = null;
  public recordCategories: Observable<RecordCategory[]> = null;
  public recordZones: Observable<RecordZone[]> = null;

  public maxLocationLength = 2000;
  public maxPerformanceLength = 2000;
  public maxCommentLength = 5000;

  // Status for the claim in GF/FSF/FAI
  // in the html-template, "not applicable" is added to the set of FAI statuses
  public statusOptions =  ["preliminary record claim received", 
                            "dossier under review",
                            "preliminary record",
                            "ratified",
                            "rejected",
                            "not applicable"];

  constructor(
      public auth: AuthenticationService,
      public recordService: RecordService,
      public route: ActivatedRoute,
      public dialog: MatDialog,
      public snackBar: MatSnackBar) { 
  }

  ngOnInit() {
    const claimId = +this.route.snapshot.paramMap.get('id');
    this.recordClasses = this.recordService.getClasses();
    this.recordCategories = this.recordService.getCategories();
    this.recordZones = this.recordService.getZones();
    this.getClaim(claimId);
    this.auth.getUser().subscribe(user => this.isLoggedIn = user !== null);
  }
  
  onSubmit() {
    if(this.isLoggedIn){
      //if(this.claimForm){      }
      if(this.claim.claimId){
        this.recordService.replaceClaim(this.claim).subscribe(data => {
          this.claim = data[0];
          const message = 'done, ' + data['affectedRows'] + ' claims was been changed.';
          this.snackBar.open(message, '', { duration: 2000 });
        });
      } else {
        this.recordService.newClaim(this.claim).subscribe(data => {
          this.claim = data[0];
          const message = 'the claims has been created.';
          this.snackBar.open(message, '', { duration: 2000 });
        });
      }
    } else {
      alert("Unauthorized operation\nPlease login");
    }
  }

  getClaim(claimId: number): void {
    this.recordService.getClaims({claimId: claimId})
      .subscribe(claim => {
        this.claim = claim[0] || nullClaim(); // this.claim can not be null, else the form bindings will fail
        this.loadSubclasses();
        this.loadTypes();
        this.loadGroups();
      });
  }
  
  onChangeClass(){
    this.claim.subclassId = null;
    this.claim.typeId = null;
    this.claim.groupId = null;
    this.loadSubclasses(); 
    this.loadTypes();
    this.loadGroups();
  }
  
  loadSubclasses(){
    if(!this.claim.classId){
      this.recordSubclasses = null;
    } else {
      this.recordSubclasses = this.recordService.getSubclasses({classId: this.claim.classId});
    }
  }
  
  loadTypes(){
    if(!this.claim.classId){
      this.recordTypes = null;
    } else {
      this.recordTypes = this.recordService.getTypes({classId: this.claim.classId});
    }
  }

  loadGroups(){
    if(!this.claim.classId){
      this.recordGroups = null;
    } else {
      this.recordGroups = this.recordService.getGroups({classId: this.claim.classId});
    }
  }
  
  onNewClass(){
    let dialogRef = this.dialog.open(RecordClassComponent);
    dialogRef.afterClosed().subscribe((result: {ok: boolean, recordClass: RecordClass }) => {
      if(result && result.ok){
        this.recordClasses = this.recordService.getClasses();
        this.claim.classId = result.recordClass.classId;
        const message = 'created class ' + result.recordClass.classAbbrev + ' - ' + result.recordClass.className;
        this.snackBar.open(message, '', { duration: 2000 });
      }
    });
  }

  onNewSubclass(){
    let dialogRef = this.dialog.open(RecordSubclassComponent, {data: {
      classId: this.claim.classId, 
      recordClasses: this.recordClasses}
    });
    dialogRef.afterClosed().subscribe((result: {ok: boolean, subclass: RecordSubclass }) => {
      if(result && result.ok){
        this.loadSubclasses();
        // if the subclass belongs to the selected class, select it in the select-option
        if(result.subclass.classId === this.claim.classId){
          this.claim.subclassId = result.subclass.subclassId;
        }
        const message = 'created subclass ' + result.subclass.subclassAbbrev + ' - ' + result.subclass.subclassName;
        this.snackBar.open(message, '', { duration: 2000 });
      }
    });
  }

  onNewType(){
    let dialogRef = this.dialog.open(RecordTypeComponent, {data: {
      classId: this.claim.classId, 
      recordClasses: this.recordClasses}
    });
    dialogRef.afterClosed().subscribe((result: {ok: boolean, type: RecordType }) => {
      if(result && result.ok){
        this.loadTypes();
        // if the subclass belongs to the selected class, select it in the select-option
        if(result.type.classId === this.claim.classId){
          this.claim.typeId = result.type.typeId;
        }
        const message = 'created type ' + result.type.typeText + '.';
        this.snackBar.open(message, '', { duration: 2000 });
      }
    });
  }
  
  onNewGroup(){
    let dialogRef = this.dialog.open(RecordGroupComponent, {data: {
      classId: this.claim.classId, 
      recordClasses: this.recordClasses}
    });
    dialogRef.afterClosed().subscribe((result: {ok: boolean, group: RecordGroup }) => {
      if(result && result.ok){
        this.loadGroups();
        // if the subclass belongs to the selected class, select it in the select-option
        if(result.group.classId === this.claim.classId){
          this.claim.groupId = result.group.groupId;
        }
        const message = 'created group ' + result.group.groupText + '.';
        this.snackBar.open(message, '', { duration: 2000 });
      }
    });
  }
}

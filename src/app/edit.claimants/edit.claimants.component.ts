import { Club, Person, ClaimantTree, ClaimTree, Claimant } from '../types/record.types';
import { RecordService } from '../record.service';
import { Component, Input, ViewChild } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { MatSnackBar, MatDialog } from "@angular/material";
import { FormControl } from "@angular/forms";
import { AuthenticationService } from "../auth/auth.service";
import { PersonComponent } from "../record-components/person/person.component";
import { ConfirmDialogComponent } from "../dialogs/confirm.dialog";

@Component({
    selector: 'edit-claimants',
    templateUrl: './edit.claimants.component.html'
})

export class EditClaimantsComponent {
  @ViewChild('claimantForm') form;
  public theClaimId: number;

// these object encapsulate all data autocomplete inputs: person and club.
  public clubInput = {
       filter: "",
       filteredList: [],  // all clubs matching the text written in the club input field
       allList: []        // the list of clubs fetched from the server
  }
  public personInput = {
      filter: "",
      filteredList: [],  // all clubs matching the text written in the club input field
      allList: []        // the list of clubs fetched from the server
  }

  // the claimants connected to this claim (stored in the database)
  public claimants: Claimant[] = null;

  @Input() set claimId(claimId : number) {
    if(claimId){
      this.theClaimId = claimId;
      this.recordService.getClaimants( {claimId: claimId} ).subscribe(data => {
        this.claimants = data;
      });
    } else {
      this.theClaimId = null;
      this.claimants = null;
    }
  }

  constructor( 
      public auth: AuthenticationService,
      public dialog: MatDialog,
      public recordService: RecordService,
      public snackBar: MatSnackBar
             ) {
  }

  ngOnInit() {
    this.recordService.getClubs().subscribe(clubList => {
      this.clubInput.allList = clubList; 
      this.clubInput.filteredList = clubList; 
    });
    this.recordService.getPersons().subscribe(personList => {
      this.personInput.allList = personList; 
      this.personInput.filteredList = personList; 
    });
  }
    
  ngAfterViewInit() {
    // register a form change observer
    this.form.control.valueChanges.subscribe(v => {
      // update the list of matching clubs
      if(this.clubInput.allList){
        this.clubInput.filteredList = this.clubInput.allList.filter((club) => {
          if(this.clubInput.filter){
            return club.name.toUpperCase().startsWith(this.clubInput.filter.toUpperCase());
          } else {
              return true;
          }
        });
      }
      // update the list of matching persons
      if(this.personInput.allList){
        this.personInput.filteredList = this.personInput.allList.filter((person) => {
          if(this.personInput.filter){
            return person.familyName.toUpperCase().startsWith(this.personInput.filter.toUpperCase());
          } else {
              return true;
          }
        });
      }
    });
  }

  onDeleteClaimant(claimId: number, personId : number, name: string) {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, {data: {
       title: 'Delete claimant', 
       msg: 'Are you sure you want to remove ' + name + ' from the claim?'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.recordService.deleteClaimant( {claimId: claimId, personId: personId} ).subscribe(data => {
          // reload the claimants, to update view
          this.recordService.getClaimants( {claimId: claimId} ).subscribe(data => {
            this.claimants = data;
          });
          // show confirmation snack bar
          const message = 'deleted ' + data.affectedRows + ' claims';
          this.snackBar.open(message, '', { duration: 2000 });
        });
      }
    });
  }
  
  public addClaimant(){
    // --- club ------
    var clubId = 0;
    var clubName = this.clubInput.filter;
    var i = 0;
    while(i < this.clubInput.allList.length && this.clubInput.allList[i].name !== clubName){
      i++;
    }
    if(i < this.clubInput.allList.length){
     clubId = this.clubInput.allList[i].clubId;
    } else {
        alert("unknown club");
        return;
    }
    // --- person ------
    var personId = 0;
    var personName = this.personInput.filter;
    var i = 0;
    while(i < this.personInput.allList.length && this.makePersonName(this.personInput.allList[i]) !== personName){
      i++;
    }
    if(i < this.personInput.allList.length){
     personId = this.personInput.allList[i].personId;
    } else {
        alert("unknown person, syntax: 'familyName, givenName (dateOfBirth)'");
        return;
    }
    //--- check if the person is already a claimant of this claim
    this.claimants.forEach(person => {
      if(person.personId === personId){
        alert(personName + " is already a claimant of this claim.");
        return;
      }
    });
 
    //--- check so we have a cliam
    if(!this.theClaimId){
      alert("Save the claim befor adding claimants");
      return;
    }
    
    this.recordService.newClaimant({claimId: this.theClaimId, personId: personId, clubId: clubId}).subscribe(data => {
      //this.claimants = data;
      // reload the claimants, to update view
      this.recordService.getClaimants( {claimId: this.theClaimId} ).subscribe(data => {
        this.claimants = data;
      });
      const message = this.personInput.filter + ' has been added to the claim.';
      this.snackBar.open(message, '', { duration: 2000 });
//      this.personInput.filter = "";
//      this.clubInput.filter = "";
      this.form.reset();
    });
}

  onNewPerson(){
    let dialogRef = this.dialog.open(PersonComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result && result.ok){
        this.recordService.getPersons().subscribe(personList => {
          this.personInput.allList = personList; 
          this.personInput.filteredList = personList; 
          this.personInput.filter = this.makePersonName(result.person);
        });
      }
    });
  }
  
  private makePersonName(person: Person): string {
    return person.familyName + ', ' + person.givenName + ' (' + person.dateOfBirth + ')';
  }
  
  sortData($event){
    console.log("sorting data" + JSON.stringify($event));
  }
}

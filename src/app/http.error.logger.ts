import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { HttpResponse } from "@angular/common/http";
import "rxjs/add/operator/do";
import { HttpErrorResponse } from "@angular/common/http";

/**
 * HttpErrorLogger intersept all http responses and logga any error message to the console.
 * An alert message is also shown to the user.
 * 
 * Three kind of errors are logged:
 * 1, the body contains a json object dith success === false.
 * 2, http error (status != 200)
 * 3, successfull http request, but the response do not contain a json object (PHP parse error)
 */

@Injectable()
export class HttpErrorLogger implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const started = Date.now();
    return next.handle(req).do(event => {
      // check if the response body contains an error message
      if (event instanceof HttpResponse && !event.body.ok){
        let msg = "http error for " + event.url + " \n";
        console.error(msg + event.body.developerErrorMessage);
        alert(msg + event.body.userErrorMessage);
      }
    }, 
    err => {
      if (err instanceof HttpErrorResponse) {
        if(err.status === 200 || err.status === 404){
          // the response body do not contain json data, most likely due to a PHP parse error
          const msg = "http error for " + err.url + " " + this.stripHTML(err.error.text);
          alert(msg);
          throw msg;
        } else {
          const msg = "http error for " + err.url;
          let body;
          try{
            body = JSON.parse(err.error);
          } catch( e ) {
            body = err.error;
          }
          console.error(msg + "\n" + (err.error.developerErrorMessage) + "\n" + (err.error.stackTrace));
          alert(msg + "\n" + body.userErrorMessage);
        }
      }
    });
  }
  
  private stripHTML(html : string): string {
     var tmp = document.createElement("DIV");
     tmp.innerHTML = html;
     return tmp.textContent || tmp.innerText || "";
  }
}
import { Component, OnInit, Inject } from '@angular/core';
import { Claim, RecordClass, RecordSubclass, RecordType, RecordGroup, RecordCategory, RecordZone } from "../types/record.types";
import { RecordService } from "../record.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmDialogComponent } from "../dialogs/confirm.dialog";
import { AuthenticationService } from "../auth/auth.service";

@Component({
  selector: 'record-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public claims: Observable<Claim[]> = null;
  private recordClasses: RecordClass[] = <RecordClass[]>[];
  private recordSubclasses: RecordSubclass[] = <RecordSubclass[]>[];
  private recordTypes: RecordType[] = <RecordType[]>[];
  private recordGroups: RecordGroup[] = <RecordGroup[]>[];
  private recordCategories: RecordCategory[] = <RecordCategory[]>[];
  private recordZones: RecordZone[] = <RecordZone[]>[];
  private isLoggenIn = false;

  constructor( public recordService: RecordService,
               public router: Router,
               public auth: AuthenticationService,
               public snackBar: MatSnackBar,
               public dialog: MatDialog
             ) {
  }

  public trunc ( str, n, useWordBoundary ){
        if (!str || str.length <= n) { 
          return str; 
        }
        var subString = str.substr(0, n-1);
        return (useWordBoundary 
           ? subString.substr(0, subString.lastIndexOf(' ')) 
           : subString) + String.fromCharCode(0x2026);
     };
     

  ngOnInit() {
    this.recordService.getClasses().subscribe(data => data.forEach(rc => this.recordClasses[rc.classId]=rc ) );
    this.recordService.getSubclasses().subscribe(data => data.forEach(val => this.recordSubclasses[val.subclassId]=val ) );
    this.recordService.getTypes().subscribe(data => data.forEach(val => this.recordTypes[val.typeId]=val ) );
    this.recordService.getCategories().subscribe(data => data.forEach(val => this.recordCategories[val.categoryId]=val ) );
    this.recordService.getGroups().subscribe(data => data.forEach(val => this.recordGroups[val.groupId]=val ) );
    this.recordService.getZones().subscribe(data => data.forEach(val => this.recordZones[val.zoneId]=val ) );

    this.claims = this.recordService.getClaims();
    this.auth.getUser().subscribe(user => this.isLoggenIn = user !== null);
    
  }

  makeClass(classId: number){
    let result = 'loading';
    const recordClass = this.recordClasses[classId];
    if(recordClass){
      result = recordClass.classAbbrev + ' - ' + recordClass.className;
    }
    return result
  }  

  makeSubclass(subclassId: number){
    let result = 'loading';
    const recordSubclass = this.recordSubclasses[subclassId];
    if(recordSubclass){
      result = recordSubclass.subclassAbbrev + ' - ' + recordSubclass.subclassName;
    }
  }  

  onEditClaim(claimId: number) {
    this.router.navigate(['/showClaim', claimId]);
  }

  onDeleteClaim(claimId: number) {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, {data: {
      title: 'Delete claim', 
      msg: 'Are you sure you want to delete the claim?'}
   });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.recordService.deleteClaim( {claimId: claimId} ).subscribe(data => {
          // reload the claims, to update view
          this.claims = this.recordService.getClaims();
          // show confirmation snack bar
          const message = 'deleted ' + data.affectedRows + ' claims';
          this.snackBar.open(message, '', { duration: 2000 });
        });
      }
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { Person, nullPerson } from "../../types/record.types";
import { RecordService } from "../../record.service";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: 'air-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  
  public person: Person = nullPerson();

  constructor(
      public recordService: RecordService,
      public dialogRef: MatDialogRef<PersonComponent>){
  }


  ngOnInit() {
  }
  
  onSubmit(){
    this.recordService.newPerson(this.person).subscribe(data => {
      this.dialogRef.close({ok: true, person: data[0]})
    });
  }

}

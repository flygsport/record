import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordSubclassComponent } from './record-subclass.component';

describe('RecordSubclassComponent', () => {
  let component: RecordSubclassComponent;
  let fixture: ComponentFixture<RecordSubclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordSubclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordSubclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

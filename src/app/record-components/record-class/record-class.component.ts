import { Component, OnInit, Inject } from '@angular/core';
import { RecordClass } from "../../types/record.types";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { RecordService } from "../../record.service";

@Component({
  selector: 'fsf-record-subclass',
  templateUrl: './record-class.component.html',
  styleUrls: ['./record-class.component.scss']
})
export class RecordClassComponent implements OnInit {

  recordClass: RecordClass = {classId: 0,   classAbbrev: null, className: null};

  constructor(
      public recordService: RecordService,
      public dialogRef: MatDialogRef<RecordClassComponent>){
  }

  ngOnInit() { }
  
  onSubmit(){
    this.recordService.newClass(this.recordClass).subscribe(data => {
      this.dialogRef.close({ok: true, recordClass: data[0]})
    });
  }

}

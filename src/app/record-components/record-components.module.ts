import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecordSubclassComponent } from './record-subclass/record-subclass.component';
import { FormsModule } from "@angular/forms";
import { MatButtonModule, MatInputModule, MatSelectModule, MatDatepickerModule, MatDialogModule } from "@angular/material";
import { RecordTypeComponent } from "./record-type/record-type.component";
import { RecordGroupComponent } from "./record-group/record-group.component";
import { RecordClassComponent } from "./record-class/record-class.component";
import { PersonComponent } from './person/person.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
              
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule

  ],
  declarations:    [ PersonComponent, RecordClassComponent, RecordGroupComponent, RecordSubclassComponent, RecordTypeComponent ],
  entryComponents: [ PersonComponent, RecordClassComponent, RecordGroupComponent, RecordSubclassComponent, RecordTypeComponent ],
  exports:         [ PersonComponent, RecordClassComponent, RecordGroupComponent, RecordSubclassComponent, RecordTypeComponent ]
})
export class RecordModule { }

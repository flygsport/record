import { Component, OnInit, Inject } from '@angular/core';
import { RecordClass, RecordGroup } from "../../types/record.types";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { RecordService } from "../../record.service";

@Component({
  selector: 'fsf-record-group',
  templateUrl: './record-group.component.html',
  styleUrls: ['./record-group.component.scss']
})
export class RecordGroupComponent implements OnInit {

  group: RecordGroup = {groupId: 0,   classId: 0, groupText: null};

  constructor(
      public recordService: RecordService,
      public dialogRef: MatDialogRef<RecordGroupComponent>,
      @Inject(MAT_DIALOG_DATA) public input: {classId: number, recordClasses: Observable<RecordClass[]>}) {
  }

  ngOnInit() {
    this.group.classId = this.input.classId;
  }
  
  onSubmit(){
    this.recordService.newGroup(this.group).subscribe(data => {
      this.dialogRef.close({ok: true, group: data[0]})
    });
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { RecordSubclass, RecordClass } from "../../types/record.types";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { RecordService } from "../../record.service";

@Component({
  selector: 'fsf-record-subclass',
  templateUrl: './record-subclass.component.html',
  styleUrls: ['./record-subclass.component.scss']
})
export class RecordSubclassComponent implements OnInit {

  subclass: RecordSubclass = {subclassId: 0,   classId: 0, subclassAbbrev: null, subclassName: null};

  constructor(
      public recordService: RecordService,
      public dialogRef: MatDialogRef<RecordSubclassComponent>,
      @Inject(MAT_DIALOG_DATA) public input: {classId: number, recordClasses: Observable<RecordClass[]>}) {
  }

  ngOnInit() {
    this.subclass.classId = this.input.classId;
  }
  
  onSubmit(){
    // TODO send to database
    this.recordService.newSubclass(this.subclass).subscribe(data => {
      this.dialogRef.close({ok: true, subclass: data[0]})
    });
  }

}

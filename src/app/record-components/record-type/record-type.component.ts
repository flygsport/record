import { Component, OnInit, Inject } from '@angular/core';
import { RecordClass, RecordType } from "../../types/record.types";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { RecordService } from "../../record.service";

@Component({
  selector: 'fsf-record-type',
  templateUrl: './record-type.component.html',
  styleUrls: ['./record-type.component.scss']
})
export class RecordTypeComponent implements OnInit {

  type: RecordType = {typeId: 0,   classId: 0, typeText: null};

  constructor(
      public recordService: RecordService,
      public dialogRef: MatDialogRef<RecordTypeComponent>,
      @Inject(MAT_DIALOG_DATA) public input: {classId: number, recordClasses: Observable<RecordClass[]>}) {
  }

  ngOnInit() {
    this.type.classId = this.input.classId;
  }
  
  onSubmit(){
    // TODO send to database
    this.recordService.newType(this.type).subscribe(data => {
      this.dialogRef.close({ok: true, type: data[0]})
    });
  }

}

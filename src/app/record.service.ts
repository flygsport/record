import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map'

import {RecordClass, RecordSubclass, RecordType, RecordCategory, RecordZone, RecordGroup, Claim, Club, Person, Claimant, ClaimTree, ClaimantTree} from './types/record.types';

interface HTTPResponse<T>{
  ok: boolean,
  data: T,
  // Only when ok === false
  userErrorMessage?: string,
      developerErrorMessage?: string,
          stackTrace?: string,
              url?: string
}

@Injectable()
export class RecordService {
  toHttpParams(params) {
    return Object.getOwnPropertyNames(params)
    .reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }
  
  constructor(private http: HttpClient) {  }
  
//=== Claim ======
  deleteClaim(parameters?: {claimId: number}): Observable<{affectedRows: number}> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.delete<HTTPResponse<{affectedRows: number}>>('/record/api/Claim.php', options).map(data => data.data );
  }
  
  getClaims(parameters?: any): Observable<Claim[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<Claim[]>>('/record/api/Claim.php', options).map(data => data.data );
  }
  
  newClaim(claim: Claim): Observable<Claim[]> {
    return this.http.post<HTTPResponse<Claim[]>>('/record/api/Claim.php', claim).map(data => data.data );
  }
  
  replaceClaim(claim: Claim): Observable<Claim[]> {
    return this.http.put<HTTPResponse<Claim[]>>('/record/api/Claim.php', claim).map(data => data.data );
  }
  
//=== Claimant ======
  deleteClaimant(parameters?: {claimId: number, personId: number}): Observable<{affectedRows: number}> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.delete<HTTPResponse<{affectedRows: number}>>('/record/api/Claimant.php', options).map(data => data.data );
  }
  
  getClaimants(parameters?: any): Observable<Claimant[]>{
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<Claimant[]>>('/record/api/Claimant.php', options).map(data => data.data );
  }
  newClaimant(claimant: Claimant): Observable<Claimant[]>{
    return this.http.post<HTTPResponse<Claimant[]>>('/record/api/Claimant.php', claimant).map(data => data.data );
  }
  
  //=== Class ===
  getClasses(parameters?: any): Observable<RecordClass[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordClass[]>>('/record/api/RecordClass.php', options).map(data => data.data );
  }
  newClass(recordClass: RecordClass): Observable<RecordClass[]>{
    return this.http.post<HTTPResponse<RecordClass[]>>('/record/api/RecordClass.php', recordClass).map(data => data.data );
  }
  
  //=== Subclass ===
  getSubclasses(parameters?: any): Observable<RecordSubclass[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordSubclass[]>>('/record/api/RecordSubclass.php', options).map(data => data.data );
  }
  newSubclass(subclass: RecordSubclass): Observable<RecordSubclass[]>{
    return this.http.post<HTTPResponse<RecordSubclass[]>>('/record/api/RecordSubclass.php', subclass).map(data => data.data );
  }

  //=== Type ===
  getTypes(parameters?: any): Observable<RecordType[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordType[]>>('/record/api/RecordType.php', options).map(data => data.data );
  }
  newType(type: RecordType): Observable<RecordType[]>{
    return this.http.post<HTTPResponse<RecordType[]>>('/record/api/RecordType.php', type).map(data => data.data );
  }
  
  //=== Group ===
  getGroups(parameters?: any): Observable<RecordGroup[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordGroup[]>>('/record/api/RecordGroup.php', options).map(data => data.data );
  }

  newGroup(group: RecordGroup): Observable<RecordGroup[]>{
    return this.http.post<HTTPResponse<RecordGroup[]>>('/record/api/RecordGroup.php', group).map(data => data.data );
  }
  
  //=== Category ===
  getCategories(parameters?: any): Observable<RecordCategory[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordCategory[]>>('/record/api/RecordCategory.php', options).map(data => data.data );
  }
  
  //=== Zone ===
  getZones(parameters?: any): Observable<RecordZone[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<RecordZone[]>>('/record/api/RecordZone.php', options).map(data => data.data );
  }
  
  //=== Club ===
  getClubs(parameters?: any): Observable<Club[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<Club[]>>('/record/api/RecordClub.php', options).map(data => data.data );
  }
  
  //=== Person ===
  getPersons(parameters?: any): Observable<Person[]> {
    let options = {};
    if(parameters){
      const httpParams = this.toHttpParams(parameters);
      options = {params: httpParams};
    }
    return this.http.get<HTTPResponse<Person[]>>('/record/api/Person.php', options).map(data => data.data );
  }
  newPerson(person: Person): Observable<Person[]>{
    person['faiLicence'] = null; // patch, the column exists in the database and POST in CRUDOperation.php needs a value for it
    return this.http.post<HTTPResponse<Person[]>>('/record/api/Person.php', person).map(data => data.data );
  }

  //--- old stuff ------------------------------
  private load(url, foo, parameters?) {
    const httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    
    return this.http.get(url, httpOptions)
    .subscribe(data => {
      if(data['ok']){
        foo(data['data']);
      }                    
    }
    );
  }
  
  newCategory(foo, data:RecordCategory){
    this.load('NewCategory', foo, data);
  }
  
  newZone(foo, data:RecordZone){
    this.load('NewZone', foo, data);
  }
  
  newClub(foo, data:Club){
    this.load('NewClub', foo, data);
  }
}

import { Club, Person, ClaimantTree, ClaimTree, Claimant } from '../types/record.types';
import { RecordService } from '../record.service';
import { Component } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'show-claimants',
    inputs: ['claimId', 'showPnr'],
    templateUrl: './show.claimants.component.html'
})

export class ShowClaimComponent {
  private claimId : number;
  private showPnr : boolean;
  public claimants : Observable<Claimant[]> = null;

  constructor( private _recordService: RecordService
             ) {
  }

  ngOnInit() {
    this.claimants = this._recordService.getClaimants( {claimId: this.claimId} );
  }
    
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavGroupComponent } from './side-nav-group.component';

describe('SideNavGroupComponent', () => {
  let component: SideNavGroupComponent;
  let fixture: ComponentFixture<SideNavGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSidenav } from "@angular/material";

@Component({
  selector: 'air-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
    @ViewChild('sidenav') sidenav: MatSidenav;
    navMode = 'side';

    ngOnInit() {
        if (window.innerWidth < 768) {
          this.navMode = 'over';
        }
      }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 768) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 768) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
    
    toggle(){
        this.sidenav.toggle();
    }
}

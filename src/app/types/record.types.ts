export interface RecordClass {
  classId: number,
  classAbbrev: string,
  className: string
}

export interface RecordSubclass {
  subclassId: number,
  classId: number,
  subclassAbbrev: string,
  subclassName: string
}

export interface RecordType {
  typeId: number,
  classId: number,
  typeText: string
}

export interface RecordGroup {
  groupId: number,
  classId: number,
  groupText: string
}

export interface RecordCategory {
  categoryId: number,
  categoryText: string
}

export interface RecordZone {
  zoneId: number,
  zoneText: string
}

export interface Claim {
    claimId: number,
    classId: number,
    subclassId: number,
    typeId: number,
    categoryId: number,
    zoneId: number,
    groupId: number,

    performanceDate: string,
    claimNbrGF: string,
    claimNbrFSF: string,
    claimNbrFAI: string,

    statusGF: string,
    statusFSF: string,
    statusFAI: string,
    currentRecord: string,

    location: string,
    performance: string,
    comment: string,

    withinSweden: boolean,
    firstRecord: boolean,
    creationTime: string,
    lastEdit: string

}

export class ClaimTree {
  constructor(
    public claimId: number,
    public recordClass: RecordClass,
    public recordSubclass: RecordSubclass,
    public type: RecordType,
    public category: RecordCategory,
    public zone: RecordZone,
    public group: RecordGroup,

    public performanceDate: string,
    public claimNbrGF: string,
    public claimNbrFSF: string,
    public claimNbrFAI: string,

    public statusFSF: string,
    public statusFAI: string,
    public currentRecord: string,

    public location: string,
    public performance: string,

    public withinSweden: boolean,
    public firstRecord: boolean

  ) { }
}

export class Club {
  constructor(
    public clubId: number,
    public clubIdIOL: string,
    public name: string,
    public startDate: string,
    public shutDownDate: string
  ) { }
}

export interface Person {
  personId: number,
  givenName: string,
  familyName: string,
  dateOfBirth: string,
  pnrDigits: string,
  faiId: string,
  iolId: string,
  clubId: number
}

export class Claimant {
  constructor(
    public claimId: number,
    public personId: number,
    public clubId: number
  ) { }
}

export class ClaimantTree {
  constructor(
    public claim: ClaimTree,
    public personId: Person,
    public clubId: Club
  ) { }
}

//==== null objects ===========================================================
export function nullClaim(): Claim {
  return {
    claimId: null,
    classId: null,
    subclassId: null,
    typeId: null,
    categoryId: null,
    zoneId: null,
    groupId: null,

    performanceDate: null,
    claimNbrGF: null,
    claimNbrFSF: null,
    claimNbrFAI: null,

    statusGF: null,
    statusFSF: null,
    statusFAI: null,
    currentRecord: null,

    location: null,
    performance: null,
    comment: null,

    withinSweden: false,
    firstRecord: false,
    creationTime: null,
    lastEdit: null
  };
}


export function nullPerson() {
  return {
    personId: 0,
    givenName: null,
    familyName: null,
    dateOfBirth: null,
    pnrDigits: null,
    faiId: null,
    iolId: null,
    clubId: 0
  };
}
  


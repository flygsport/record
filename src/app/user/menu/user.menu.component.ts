import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


import { AuthenticationService } from "../../auth/auth.service";

@Component({
  selector: 'air-user-menu',
  templateUrl: './user.menu.component.html',
  styleUrls: ['./user.menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  user;
  data;

  constructor(public auth: AuthenticationService, 
              public dialog: MatDialog) { }

  ngOnInit() {
    this.auth.getUser().subscribe(user => this.user = user);
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(LoginDialogComponent);
  }
}


@Component({
    selector: 'login-dialog',
    templateUrl: 'login.dialog.html',
  })
  export class LoginDialogComponent {
     data = {user: '', password: ''};

    constructor(
      public dialogRef: MatDialogRef<LoginDialogComponent>,
      public auth: AuthenticationService) { 
      
    }

    login(data): void {
      this.auth.login(data.user, data.password).subscribe(
          data => { if(data){ this.dialogRef.close(); } },
          err => alert('login failed') );
    }

  }


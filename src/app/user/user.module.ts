import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserMenuComponent, LoginDialogComponent } from './menu/user.menu.component';

import { MatDialogModule, MatInputModule, MatMenuModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    
    MatDialogModule, 
    MatInputModule,
    MatMenuModule
  ],
  declarations: [ UserMenuComponent, LoginDialogComponent ],
  entryComponents: [ LoginDialogComponent ],
  exports:      [ UserMenuComponent ]
})

export class UserModule { }
